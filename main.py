import discord
import os
from dotenv import load_dotenv

load_dotenv()

serverID = int(os.getenv('SERVER_ID'))
adminID = int(os.getenv('ADMIN_ID')) 
vettingID = int(os.getenv('VETTING_CHANNEL_ID'))
inducteeID = int(os.getenv('INDUCTEE_ID'))
memberID = int(os.getenv('MEMBER_ID'))

intents = discord.Intents.default()
intents.members = True
intents.guilds = True

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    global guild
    global adminRole
    global inductee
    global fullmember

    guild = client.get_guild(serverID)
    adminRole = guild.get_role(adminID)
    inductee = guild.get_role(inducteeID)
    fullmember = guild.get_role(memberID)
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_reaction_add(reaction, user):
    if reaction.message.channel.id == vettingID and adminRole in user.roles and reaction.emoji == '🆗' and len(reaction.message.embeds) == 1 and reaction.message.reactions[1].count == 1:
            author = reaction.message.embeds[0].author
            name = str(getattr(author, 'name'))
            splitname = name.split('#')
            VettedUser = discord.utils.get(guild.members, name=splitname[0], discriminator=splitname[1])

            await VettedUser.remove_roles(inductee)
            await VettedUser.add_roles(fullmember)

@client.event
async def on_thread_join(thread):
    prevBotMsg = await thread.history().get(author__id=client.user.id)
    if prevBotMsg is None:
        await thread.send("New thread <@&{}>".format(adminID))

token = os.getenv('TOKEN')

client.run(token)